import React from "react";
import template from "../templates/MainInput.pug";
import "../styles/MainInput.sass";
import { writeAction } from "../redux/actions/WriteFile";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../redux/store/store";
import ContentEditable from 'react-contenteditable'


export type MainInputProps = {
}

const MainInput: React.FC<MainInputProps> = (props) => {
    const showdown = require('showdown');
    const converter = new showdown.Converter();
    const editor = useSelector((state: RootState)  => state.editor);
    const dispatch = useDispatch();
    const onChange = (event: any) => {
        if(event.type === "input") {
            const newContent = event.currentTarget.innerText;
            const newHTML = event.currentTarget.innerHTML;
            dispatch(writeAction(newContent, newHTML));
        }
    };
    const fileId = editor.currentFile;
    let data = "";
    let fileName = "";
    let preview = "";
    let editView = "";
    if(fileId !== -1) {
        data = editor.files[fileId].content;
        editView = editor.files[fileId].html;
        fileName = editor.files[fileId].fileName;
        if(!data) {
            data = "";
        }
        preview = converter.makeHtml(data);

    }
    return template.call({},{
        ContentEditable,

        data,
        editView,
        preview,
        onChange,
        fileName
    })
};

export default MainInput;