import React, {useState} from "react";
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import template from "../templates/Sidebar.pug";
import '../styles/Sidebar.sass';
import CheckboxTree from 'react-checkbox-tree';
import {useDispatch} from "react-redux";
import {openAction} from "../redux/actions/OpenFile";

const nodes = [{
    value: 0,
    label: 'Mars',
    children: [
        { value: 1, label: 'Phobos', },
        { value: 2, label: 'Deimos' },
    ],
}];


const Sidebar:React.FC = (props) => {
    const i:any[] = [];
    const [expanded, setExpanded] = useState(i);
    const [checked, setChecked] = useState(i);
    const dispatch = useDispatch();

    const onCheck = (checked: any) => {
        setChecked(checked);
    };

    const onExpand = (expanded: any) => {
        setExpanded(expanded);
    };

    const onClick = (targetNode: any) => {
        console.log(targetNode);
        if(!targetNode.isLeaf) {
            let newExpanded;
            if(expanded.includes(targetNode.value)) {
                newExpanded = expanded.filter((value: string) => (value !== targetNode.value));
            }else {
                newExpanded = [...expanded, targetNode.value];
            }
            setExpanded(newExpanded);
        }else {
            dispatch(openAction(targetNode.value, targetNode.label));
        }
    };

    return template.call({}, {
        CheckboxTree,
        nodes,
        expanded,
        checked,
        onCheck,
        onExpand,
        onClick
    })
};


export default Sidebar;