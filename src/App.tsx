import React from 'react';
import './styles/App.sass';
import template from './templates/App.pug';
import Header from './components/Header';
import Sidebar from "./components/Sidebar";
import MainInput from "./components/MainInput";

const App: React.FC = () => {
    return template.call({}, {
        Header,
        Sidebar,
        MainInput
    });
};

export default App;
