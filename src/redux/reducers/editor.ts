import {OPEN_ACTION, OPEN_FILE, WRITE_ACTION, WRITE_FILE} from "../actions/actionTypes";
import {EditorState} from "../store/types";

const initialSate: EditorState = {
    files: {},
    currentFile: -1,
};

const write = (state:EditorState, action: WRITE_ACTION): EditorState => {
    let newFiles = state.files;
    newFiles[state.currentFile] = {content: action.payload.content, html: action.payload.html,fileName: state.files[state.currentFile].fileName};
    return {
        files: newFiles,
        currentFile: state.currentFile
    };
};

const open = (state:EditorState, action: OPEN_ACTION): EditorState => {
    let newFiles = state.files;
    if(!(action.payload.fileId in newFiles)) {
        newFiles[action.payload.fileId] = {content: "", html: "", fileName: action.payload.fileName};
    }
    return {
        files: newFiles,
        currentFile: action.payload.fileId
    };
};

const editor = (state: EditorState = initialSate, action: WRITE_ACTION|OPEN_ACTION): EditorState => {
    let newState: EditorState = state;
    switch (action.type) {
        case WRITE_FILE:
            newState = write(state, action);
            break;
        case OPEN_FILE:
            newState = open(state, action);
            break;
    }
    return newState;
};

export default editor;