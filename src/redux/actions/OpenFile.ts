import {OPEN_ACTION, OPEN_FILE} from "./actionTypes";

export const openAction = (fileId: number, fileName: string): OPEN_ACTION => ({
    type: OPEN_FILE,
    payload: {
        fileId: fileId,
        fileName: fileName
    }
});