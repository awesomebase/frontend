import {WRITE_ACTION, WRITE_FILE} from "./actionTypes";

export const writeAction = (content: string, html: string): WRITE_ACTION => ({
    type: WRITE_FILE,
    payload: {
        content: content,
        html: html
    }
});