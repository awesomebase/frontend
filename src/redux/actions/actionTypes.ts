export const WRITE_FILE = "WRITE_FILE";
export const OPEN_FILE = "OPEN_FILE";

interface WRITE_ACTION_TYPE {
    type: typeof WRITE_FILE,
    payload: {
        content: string,
        html: string
    }
}

interface OPEN_FILE_ACTION {
    type: typeof OPEN_FILE,
    payload: {
        fileId: number,
        fileName: string
    }
}


export type WRITE_ACTION = WRITE_ACTION_TYPE;
export type OPEN_ACTION = OPEN_FILE_ACTION;