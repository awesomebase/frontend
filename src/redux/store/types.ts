export interface EditorState {
    files: {
        [name: number]: {
            content: string,
            html: string,
            fileName: string
        }
    }
    currentFile: number,
}